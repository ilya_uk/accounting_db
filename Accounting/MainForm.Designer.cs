﻿namespace Accounting
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_LoginSE = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button_LoginMTO = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_LoginSE);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button_LoginMTO);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(17, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(572, 212);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Вход в систему";
            // 
            // button_LoginSE
            // 
            this.button_LoginSE.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.button_LoginSE.Location = new System.Drawing.Point(69, 126);
            this.button_LoginSE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_LoginSE.Name = "button_LoginSE";
            this.button_LoginSE.Size = new System.Drawing.Size(432, 53);
            this.button_LoginSE.TabIndex = 1;
            this.button_LoginSE.Text = "Служба эксплуатации";
            this.button_LoginSE.UseVisualStyleBackColor = true;
            this.button_LoginSE.Click += new System.EventHandler(this.button_LoginSE_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(27, 117);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 2;
            // 
            // button_LoginMTO
            // 
            this.button_LoginMTO.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_LoginMTO.Location = new System.Drawing.Point(69, 43);
            this.button_LoginMTO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_LoginMTO.Name = "button_LoginMTO";
            this.button_LoginMTO.Size = new System.Drawing.Size(432, 57);
            this.button_LoginMTO.TabIndex = 0;
            this.button_LoginMTO.Text = "Материально-технический отдел";
            this.button_LoginMTO.UseVisualStyleBackColor = true;
            this.button_LoginMTO.Click += new System.EventHandler(this.button_LoginMTO_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(27, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(605, 238);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Учёт аппаратуры ООО \"Ооошечка\"";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_LoginSE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_LoginMTO;
        private System.Windows.Forms.Label label1;
    }
}

