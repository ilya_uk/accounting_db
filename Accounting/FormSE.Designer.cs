﻿namespace Accounting
{
    partial class FormSE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSE));
            this.tabControl_SE = new System.Windows.Forms.TabControl();
            this.tabPage_Catalog = new System.Windows.Forms.TabPage();
            this.button_tableCatalog = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_idCatalog = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_locationCatalog = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_majorCatalog = new System.Windows.Forms.TextBox();
            this.textBox_maskCatalog = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_typeCatalog = new System.Windows.Forms.TextBox();
            this.textBox_ipCatalog = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_nameCatalog = new System.Windows.Forms.TextBox();
            this.textBox_netCatalog = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView_Catalog = new System.Windows.Forms.DataGridView();
            this.tabPage_Computers = new System.Windows.Forms.TabPage();
            this.button_tableComputers = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox_locationComputers = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_majorComputers = new System.Windows.Forms.TextBox();
            this.textBox_maskComputers = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_netComputers = new System.Windows.Forms.TextBox();
            this.textBox_ipComputers = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button_Components = new System.Windows.Forms.Button();
            this.dataGridView_Computers = new System.Windows.Forms.DataGridView();
            this.tabControl_SE.SuspendLayout();
            this.tabPage_Catalog.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Catalog)).BeginInit();
            this.tabPage_Computers.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Computers)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl_SE
            // 
            this.tabControl_SE.Controls.Add(this.tabPage_Catalog);
            this.tabControl_SE.Controls.Add(this.tabPage_Computers);
            this.tabControl_SE.Location = new System.Drawing.Point(16, 15);
            this.tabControl_SE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl_SE.Name = "tabControl_SE";
            this.tabControl_SE.SelectedIndex = 0;
            this.tabControl_SE.Size = new System.Drawing.Size(1289, 524);
            this.tabControl_SE.TabIndex = 0;
            // 
            // tabPage_Catalog
            // 
            this.tabPage_Catalog.Controls.Add(this.button_tableCatalog);
            this.tabPage_Catalog.Controls.Add(this.groupBox1);
            this.tabPage_Catalog.Controls.Add(this.dataGridView_Catalog);
            this.tabPage_Catalog.Location = new System.Drawing.Point(4, 25);
            this.tabPage_Catalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Catalog.Name = "tabPage_Catalog";
            this.tabPage_Catalog.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Catalog.Size = new System.Drawing.Size(1281, 495);
            this.tabPage_Catalog.TabIndex = 0;
            this.tabPage_Catalog.Text = "Оборудование";
            this.tabPage_Catalog.UseVisualStyleBackColor = true;
            // 
            // button_tableCatalog
            // 
            this.button_tableCatalog.Location = new System.Drawing.Point(1189, 418);
            this.button_tableCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_tableCatalog.Name = "button_tableCatalog";
            this.button_tableCatalog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button_tableCatalog.Size = new System.Drawing.Size(79, 59);
            this.button_tableCatalog.TabIndex = 38;
            this.button_tableCatalog.Text = "Сброс";
            this.button_tableCatalog.UseVisualStyleBackColor = true;
            this.button_tableCatalog.Click += new System.EventHandler(this.button_tableCatalog_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_idCatalog);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox_locationCatalog);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox_majorCatalog);
            this.groupBox1.Controls.Add(this.textBox_maskCatalog);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox_typeCatalog);
            this.groupBox1.Controls.Add(this.textBox_ipCatalog);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox_nameCatalog);
            this.groupBox1.Controls.Add(this.textBox_netCatalog);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(4, 405);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(1177, 80);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск";
            // 
            // textBox_idCatalog
            // 
            this.textBox_idCatalog.Location = new System.Drawing.Point(8, 48);
            this.textBox_idCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_idCatalog.Name = "textBox_idCatalog";
            this.textBox_idCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_idCatalog.TabIndex = 0;
            this.textBox_idCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 25);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 17);
            this.label6.TabIndex = 36;
            this.label6.Text = "Серийный номер";
            // 
            // textBox_locationCatalog
            // 
            this.textBox_locationCatalog.Location = new System.Drawing.Point(187, 48);
            this.textBox_locationCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_locationCatalog.Name = "textBox_locationCatalog";
            this.textBox_locationCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_locationCatalog.TabIndex = 1;
            this.textBox_locationCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(188, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "Размещение";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1031, 25);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 17);
            this.label5.TabIndex = 34;
            this.label5.Text = "Маска подсети";
            // 
            // textBox_majorCatalog
            // 
            this.textBox_majorCatalog.Location = new System.Drawing.Point(328, 48);
            this.textBox_majorCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_majorCatalog.Name = "textBox_majorCatalog";
            this.textBox_majorCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_majorCatalog.TabIndex = 2;
            this.textBox_majorCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // textBox_maskCatalog
            // 
            this.textBox_maskCatalog.Location = new System.Drawing.Point(1035, 48);
            this.textBox_maskCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_maskCatalog.Name = "textBox_maskCatalog";
            this.textBox_maskCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_maskCatalog.TabIndex = 7;
            this.textBox_maskCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(329, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "Ответственный";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(889, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 17);
            this.label4.TabIndex = 32;
            this.label4.Text = "IP";
            // 
            // textBox_typeCatalog
            // 
            this.textBox_typeCatalog.Location = new System.Drawing.Point(469, 48);
            this.textBox_typeCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_typeCatalog.Name = "textBox_typeCatalog";
            this.textBox_typeCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_typeCatalog.TabIndex = 3;
            this.textBox_typeCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // textBox_ipCatalog
            // 
            this.textBox_ipCatalog.Location = new System.Drawing.Point(893, 48);
            this.textBox_ipCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_ipCatalog.Name = "textBox_ipCatalog";
            this.textBox_ipCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_ipCatalog.TabIndex = 6;
            this.textBox_ipCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(471, 25);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 17);
            this.label8.TabIndex = 26;
            this.label8.Text = "Тип";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(748, 25);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 30;
            this.label3.Text = "Сетевое имя";
            // 
            // textBox_nameCatalog
            // 
            this.textBox_nameCatalog.Location = new System.Drawing.Point(611, 48);
            this.textBox_nameCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_nameCatalog.Name = "textBox_nameCatalog";
            this.textBox_nameCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_nameCatalog.TabIndex = 4;
            this.textBox_nameCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // textBox_netCatalog
            // 
            this.textBox_netCatalog.Location = new System.Drawing.Point(752, 48);
            this.textBox_netCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_netCatalog.Name = "textBox_netCatalog";
            this.textBox_netCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_netCatalog.TabIndex = 5;
            this.textBox_netCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChange);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(612, 25);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 17);
            this.label7.TabIndex = 28;
            this.label7.Text = "Наименование";
            // 
            // dataGridView_Catalog
            // 
            this.dataGridView_Catalog.AllowUserToAddRows = false;
            this.dataGridView_Catalog.AllowUserToDeleteRows = false;
            this.dataGridView_Catalog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Catalog.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Catalog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Catalog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Catalog.Location = new System.Drawing.Point(8, 7);
            this.dataGridView_Catalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView_Catalog.MultiSelect = false;
            this.dataGridView_Catalog.Name = "dataGridView_Catalog";
            this.dataGridView_Catalog.ReadOnly = true;
            this.dataGridView_Catalog.RowHeadersWidth = 51;
            this.dataGridView_Catalog.Size = new System.Drawing.Size(1267, 388);
            this.dataGridView_Catalog.TabIndex = 8;
            this.dataGridView_Catalog.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Catalog_CellValueChanged);
            this.dataGridView_Catalog.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_Catalog_DataError);
            // 
            // tabPage_Computers
            // 
            this.tabPage_Computers.Controls.Add(this.button_tableComputers);
            this.tabPage_Computers.Controls.Add(this.groupBox2);
            this.tabPage_Computers.Controls.Add(this.button_Components);
            this.tabPage_Computers.Controls.Add(this.dataGridView_Computers);
            this.tabPage_Computers.Location = new System.Drawing.Point(4, 25);
            this.tabPage_Computers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Computers.Name = "tabPage_Computers";
            this.tabPage_Computers.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Computers.Size = new System.Drawing.Size(1281, 495);
            this.tabPage_Computers.TabIndex = 1;
            this.tabPage_Computers.Text = "Компьютеры";
            this.tabPage_Computers.UseVisualStyleBackColor = true;
            // 
            // button_tableComputers
            // 
            this.button_tableComputers.Location = new System.Drawing.Point(733, 418);
            this.button_tableComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_tableComputers.Name = "button_tableComputers";
            this.button_tableComputers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button_tableComputers.Size = new System.Drawing.Size(79, 59);
            this.button_tableComputers.TabIndex = 52;
            this.button_tableComputers.Text = "Сброс";
            this.button_tableComputers.UseVisualStyleBackColor = true;
            this.button_tableComputers.Click += new System.EventHandler(this.button_tableCatalog_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_locationComputers);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.textBox_majorComputers);
            this.groupBox2.Controls.Add(this.textBox_maskComputers);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.textBox_netComputers);
            this.groupBox2.Controls.Add(this.textBox_ipComputers);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(4, 405);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(721, 80);
            this.groupBox2.TabIndex = 51;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск";
            // 
            // textBox_locationComputers
            // 
            this.textBox_locationComputers.Location = new System.Drawing.Point(8, 48);
            this.textBox_locationComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_locationComputers.Name = "textBox_locationComputers";
            this.textBox_locationComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_locationComputers.TabIndex = 0;
            this.textBox_locationComputers.TextChanged += new System.EventHandler(this.Computers_TextChange);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 25);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 17);
            this.label16.TabIndex = 38;
            this.label16.Text = "Размещение";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(569, 25);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 17);
            this.label10.TabIndex = 50;
            this.label10.Text = "Маска подсети";
            // 
            // textBox_majorComputers
            // 
            this.textBox_majorComputers.Location = new System.Drawing.Point(149, 48);
            this.textBox_majorComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_majorComputers.Name = "textBox_majorComputers";
            this.textBox_majorComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_majorComputers.TabIndex = 1;
            this.textBox_majorComputers.TextChanged += new System.EventHandler(this.Computers_TextChange);
            // 
            // textBox_maskComputers
            // 
            this.textBox_maskComputers.Location = new System.Drawing.Point(573, 48);
            this.textBox_maskComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_maskComputers.Name = "textBox_maskComputers";
            this.textBox_maskComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_maskComputers.TabIndex = 4;
            this.textBox_maskComputers.TextChanged += new System.EventHandler(this.Computers_TextChange);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(151, 25);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 17);
            this.label15.TabIndex = 40;
            this.label15.Text = "Ответственный";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(428, 25);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 17);
            this.label11.TabIndex = 48;
            this.label11.Text = "IP";
            // 
            // textBox_netComputers
            // 
            this.textBox_netComputers.Location = new System.Drawing.Point(291, 48);
            this.textBox_netComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_netComputers.Name = "textBox_netComputers";
            this.textBox_netComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_netComputers.TabIndex = 2;
            this.textBox_netComputers.TextChanged += new System.EventHandler(this.Computers_TextChange);
            // 
            // textBox_ipComputers
            // 
            this.textBox_ipComputers.Location = new System.Drawing.Point(432, 48);
            this.textBox_ipComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_ipComputers.Name = "textBox_ipComputers";
            this.textBox_ipComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_ipComputers.TabIndex = 3;
            this.textBox_ipComputers.TextChanged += new System.EventHandler(this.Computers_TextChange);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(287, 25);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 17);
            this.label12.TabIndex = 46;
            this.label12.Text = "Сетевое имя";
            // 
            // button_Components
            // 
            this.button_Components.AutoSize = true;
            this.button_Components.Location = new System.Drawing.Point(854, 416);
            this.button_Components.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_Components.Name = "button_Components";
            this.button_Components.Size = new System.Drawing.Size(315, 61);
            this.button_Components.TabIndex = 5;
            this.button_Components.Text = "Компоненты компьютера";
            this.button_Components.UseVisualStyleBackColor = true;
            this.button_Components.Click += new System.EventHandler(this.button_Components_Click);
            // 
            // dataGridView_Computers
            // 
            this.dataGridView_Computers.AllowUserToAddRows = false;
            this.dataGridView_Computers.AllowUserToDeleteRows = false;
            this.dataGridView_Computers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Computers.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Computers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Computers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Computers.Location = new System.Drawing.Point(8, 7);
            this.dataGridView_Computers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView_Computers.MultiSelect = false;
            this.dataGridView_Computers.Name = "dataGridView_Computers";
            this.dataGridView_Computers.RowHeadersWidth = 51;
            this.dataGridView_Computers.Size = new System.Drawing.Size(1269, 388);
            this.dataGridView_Computers.TabIndex = 6;
            this.dataGridView_Computers.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Computers_CellValueChanged);
            this.dataGridView_Computers.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_Computers_DataError);
            // 
            // FormSE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1320, 556);
            this.Controls.Add(this.tabControl_SE);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "FormSE";
            this.Text = "Служба эксплуатации";
            this.Load += new System.EventHandler(this.FormSE_Load);
            this.tabControl_SE.ResumeLayout(false);
            this.tabPage_Catalog.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Catalog)).EndInit();
            this.tabPage_Computers.ResumeLayout(false);
            this.tabPage_Computers.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Computers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl_SE;
        private System.Windows.Forms.TabPage tabPage_Catalog;
        private System.Windows.Forms.TabPage tabPage_Computers;
        private System.Windows.Forms.DataGridView dataGridView_Catalog;
        private System.Windows.Forms.DataGridView dataGridView_Computers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_netCatalog;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_nameCatalog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_typeCatalog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_majorCatalog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_locationCatalog;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_idCatalog;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_maskCatalog;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_ipCatalog;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_maskComputers;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_ipComputers;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_netComputers;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox_majorComputers;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox_locationComputers;
        private System.Windows.Forms.Button button_Components;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_tableCatalog;
        private System.Windows.Forms.Button button_tableComputers;
    }
}