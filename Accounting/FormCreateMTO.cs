﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FormCreateMTO : Form
    {
        public List<string> idComputers = new List<string>();
        public List<string> idComponents = new List<string>();
        List<string> locationComponents = new List<string>();

        public FormCreateMTO()
        {
            InitializeComponent();
        }

        public void LocationComponents(List<KeyValuePair<int, string>> keyValuePairs)
        {
            textBox_location.DataSource = keyValuePairs;
            textBox_location.DisplayMember = "value";
            textBox_location.ValueMember = "key";

            foreach (var item in keyValuePairs)
            {
                locationComponents.Add(item.Value);
            }

        }

        private void button_add_Click(object sender, EventArgs e)
        {
            if (textBox_id.Text != String.Empty && textBox_name.Text != String.Empty && textBox_type.Text != String.Empty
                && textBox_seller.Text != String.Empty && textBox_price.Text != String.Empty
                && textBox_location.Text != String.Empty && textBox_major.Text != String.Empty)
            {
                if (Text == "Регистрация компьютера")
                {
                    if (!idComputers.Contains(textBox_id.Text)) DialogResult = DialogResult.OK;
                    else if(MessageBox.Show("Данный серийный номер уже существует.\nПродолжить?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) DialogResult = DialogResult.OK;
                }
                else
                {
                    if (!idComponents.Contains(textBox_id.Text))
                    {
                        if (locationComponents.Contains(textBox_location.Text)) DialogResult = DialogResult.OK;
                        else MessageBox.Show("Не существующее размещение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else if (MessageBox.Show("Данный серийный номер уже существует.\nПродолжить?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) DialogResult = DialogResult.OK;
                }
            }
            else MessageBox.Show("Заполните пустые поля!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void textBox_id_TextChanged(object sender, EventArgs e)
        {
            TextBox box = (TextBox)sender;

            byte number = 0;

            for(int i = 0; i < box.Text.Length;i++)
                if(!Byte.TryParse(box.Text.ToCharArray()[i].ToString(), out number))
                {
                    errorProvider.SetError(box, "Не допустимый тип данных!");
                    box.Clear();
                    break;
                }
            else errorProvider.Clear();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
