﻿namespace Accounting
{
    partial class FormComponentsSE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormComponentsSE));
            this.dataGridView_Components = new System.Windows.Forms.DataGridView();
            this.button_addComponents = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_delete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Components)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Components
            // 
            this.dataGridView_Components.AllowUserToAddRows = false;
            this.dataGridView_Components.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Components.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Components.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Components.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Components.Location = new System.Drawing.Point(12, 12);
            this.dataGridView_Components.MultiSelect = false;
            this.dataGridView_Components.Name = "dataGridView_Components";
            this.dataGridView_Components.ReadOnly = true;
            this.dataGridView_Components.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_Components.Size = new System.Drawing.Size(594, 288);
            this.dataGridView_Components.TabIndex = 4;
            this.dataGridView_Components.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Catalog_CellValueChanged);
            this.dataGridView_Components.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_Components_DataError);
            this.dataGridView_Components.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView_Components_UserDeletingRow);
            // 
            // button_addComponents
            // 
            this.button_addComponents.AutoSize = true;
            this.button_addComponents.Location = new System.Drawing.Point(232, 306);
            this.button_addComponents.Name = "button_addComponents";
            this.button_addComponents.Size = new System.Drawing.Size(168, 23);
            this.button_addComponents.TabIndex = 0;
            this.button_addComponents.Text = "Добавить новые компоненты";
            this.button_addComponents.UseVisualStyleBackColor = true;
            this.button_addComponents.Click += new System.EventHandler(this.button_addComponents_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(513, 306);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_cancel.TabIndex = 3;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_delete
            // 
            this.button_delete.Location = new System.Drawing.Point(422, 306);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(75, 23);
            this.button_delete.TabIndex = 1;
            this.button_delete.Text = "Удалить";
            this.button_delete.UseVisualStyleBackColor = true;
            this.button_delete.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // FormComponentsSE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(619, 362);
            this.Controls.Add(this.button_delete);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_addComponents);
            this.Controls.Add(this.dataGridView_Components);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormComponentsSE";
            this.Text = "Компоненты компьютера";
            this.Load += new System.EventHandler(this.FormComponentsSE_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Components)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Components;
        private System.Windows.Forms.Button button_addComponents;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_delete;
    }
}