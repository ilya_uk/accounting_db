﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class MainForm : Form
    {
        /*
         * Навигация по нескольким полям, а редактирование по полям методички!
         * Навигацию по комплектующим в компьютере не делать!
        */
        public MainForm()
        {
            InitializeComponent();
        }

        private void button_LoginMTO_Click(object sender, EventArgs e)
        {
            LoadForm load = new LoadForm();
            if (load.ShowDialog() != DialogResult.OK)
            {
                MessageBox.Show("Ошибка подключения к базе данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.Hide();

            FormMTO formMTO = new FormMTO();
            formMTO.ShowDialog();

            this.Show();
        }

        private void button_LoginSE_Click(object sender, EventArgs e)
        {
            LoadForm load = new LoadForm();
            if (load.ShowDialog() != DialogResult.OK)
            {
                MessageBox.Show("Ошибка подключения к базе данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.Hide();

            FormSE formSE = new FormSE();
            formSE.ShowDialog();

            this.Show();
        }
    }
}
