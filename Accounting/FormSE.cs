﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Accounting
{
    public partial class FormSE : Form
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename = |DataDirectory|\Database_Equipment.mdf;Integrated Security=True";
        bool canChange = false;

        public FormSE()
        {
            InitializeComponent();
        }

        void ColorCells(DataGridView data)
        {
            for (int i = 0; i < data.RowCount; i++)
            {
                if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value < DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.Red;
                else if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value > DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkGreen;
                else data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkOrange;
            }
        }

        void DGV_ComputersUpdate()
        {
            canChange = false;

            string request = String.Format(@"
            SELECT * FROM [Computers] WHERE
            Location LIKE N'%{0}%' AND Major LIKE N'%{1}%' AND (Net_name LIKE N'%{2}%' OR Net_name IS NULL) AND (IP LIKE N'%{3}%' OR IP IS NULL)
            AND (Mask_subnet LIKE N'%{4}%' OR Mask_subnet IS NULL)",
            textBox_locationComputers.Text, textBox_majorComputers.Text, textBox_netComputers.Text, textBox_ipComputers.Text,
            textBox_maskComputers.Text);

            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(request, connection);

            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView_Computers.DataSource = table;

            dataGridView_Computers.Columns["Id"].Visible = false;

            dataGridView_Computers.Columns["S_N"].HeaderText = "Серийный номер";
            dataGridView_Computers.Columns["Name"].HeaderText = "Наименование";
            dataGridView_Computers.Columns["Date_guaranty"].HeaderText = "Истечение гарантии";

            ColorCells(dataGridView_Computers);

            dataGridView_Computers.Columns["Seller"].HeaderText = "Продавец";
            dataGridView_Computers.Columns["Type"].HeaderText = "Тип";
            dataGridView_Computers.Columns["Date_entry"].HeaderText = "Дата поступления";
            dataGridView_Computers.Columns["Price"].HeaderText = "Цена";
            dataGridView_Computers.Columns["Net_name"].HeaderText = "Сетевое имя";
            dataGridView_Computers.Columns["IP"].HeaderText = "IP";
            dataGridView_Computers.Columns["Mask_subnet"].HeaderText = "Маска подсети";
            dataGridView_Computers.Columns["Location"].HeaderText = "Размещение";
            dataGridView_Computers.Columns["Major"].HeaderText = "Ответственный";

            //dataGridView_Computers.ClearSelection();
            canChange = true;
        }

        void DGV_CatalogUpdate()
        {
            canChange = false;
            //ПОИСК ПО СЕРИЙНОМУ НОМЕРУ!!! Готово!
            string request = String.Format(@"
         (SELECT S_N, Name, Date_guaranty, Seller, Type, Date_entry, Price, Location, Major FROM [Components] 
         WHERE S_N LIKE N'{0}%' AND Name LIKE N'%{1}%' AND Type LIKE N'%{2}%' AND Location LIKE N'%{3}%' AND Major LIKE N'%{4}%') UNION 
         (SELECT S_N, Name, Date_guaranty, Seller, Type, Date_entry, Price, Location, Major FROM [Computers]
         WHERE S_N LIKE N'{0}%' AND Name LIKE N'%{1}%' AND Type LIKE N'%{2}%' AND Location LIKE N'%{3}%' AND Major LIKE N'%{4}%')",
         textBox_idCatalog.Text, textBox_nameCatalog.Text, textBox_typeCatalog.Text,
         textBox_locationCatalog.Text, textBox_majorCatalog.Text);
            //================
            if (textBox_netCatalog.Text != String.Empty || textBox_ipCatalog.Text != String.Empty || textBox_maskCatalog.Text != String.Empty)
                request = String.Format(@"SELECT * FROM [Computers]
         WHERE Net_name LIKE N'%{0}%' AND IP LIKE N'%{1}%' AND Mask_subnet LIKE N'%{2}%' AND
         S_N LIKE N'{3}%' AND Name LIKE N'%{4}%' AND Type LIKE N'%{5}%' AND Location LIKE N'%{6}%' AND Major LIKE N'%{7}%'",
         textBox_netCatalog.Text, textBox_ipCatalog.Text, textBox_maskCatalog.Text,
         textBox_idCatalog.Text, textBox_nameCatalog.Text, textBox_typeCatalog.Text,
         textBox_locationCatalog.Text, textBox_majorCatalog.Text);
            //=================
            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(request, connection);

            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView_Catalog.DataSource = table;

            //============
            if (textBox_netCatalog.Text != String.Empty || textBox_ipCatalog.Text != String.Empty || textBox_maskCatalog.Text != String.Empty)
            {
                dataGridView_Catalog.Columns["Id"].Visible = false;
                dataGridView_Catalog.Columns["Net_name"].HeaderText = "Сетевое имя";
                dataGridView_Catalog.Columns["IP"].HeaderText = "IP";
                dataGridView_Catalog.Columns["Mask_subnet"].HeaderText = "Маска подсети";
            }
            //====================

            dataGridView_Catalog.Columns["S_N"].HeaderText = "Серийный номер";
            dataGridView_Catalog.Columns["Name"].HeaderText = "Наименование";
            dataGridView_Catalog.Columns["Date_guaranty"].HeaderText = "Истечение гарантии";

            ColorCells(dataGridView_Catalog);

            dataGridView_Catalog.Columns["Seller"].HeaderText = "Продавец";
            dataGridView_Catalog.Columns["Type"].HeaderText = "Тип";
            dataGridView_Catalog.Columns["Date_entry"].HeaderText = "Дата поступления";
            dataGridView_Catalog.Columns["Price"].HeaderText = "Цена";
            dataGridView_Catalog.Columns["Location"].HeaderText = "Размещение";
            dataGridView_Catalog.Columns["Major"].HeaderText = "Ответственный";

            //dataGridView_Catalog.ClearSelection();
            canChange = true;
        }

        private void FormSE_Load(object sender, EventArgs e)
        {
            tabControl_SE.SelectedIndex = 1;
            DGV_ComputersUpdate();
            dataGridView_Computers.Select();

            tabControl_SE.SelectedIndex = 0;
            DGV_CatalogUpdate();
            dataGridView_Catalog.Select();
        }

        private void Catalog_TextChange(object sender, EventArgs e)
        {
            DGV_CatalogUpdate();
        }

        private void Computers_TextChange(object sender, EventArgs e)
        {
            DGV_ComputersUpdate();
        }

        private void dataGridView_Computers_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!canChange) return;

            if (e.ColumnIndex == 8)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Computers] SET Location = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else if (e.ColumnIndex == 9)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Computers] SET Major = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else if (e.ColumnIndex == 10)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Computers] SET Net_name = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else if (e.ColumnIndex == 11)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Computers] SET IP = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else if (e.ColumnIndex == 12)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Computers] SET Mask_subnet = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else
            {
                MessageBox.Show("Редактирование недоступно", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                DGV_ComputersUpdate();
            }

            DGV_CatalogUpdate();
        }

        private void button_Components_Click(object sender, EventArgs e)
        {
            int id = 0;
            string sn = String.Empty;

            bool canCreate = true;

            try
            {
                id = (int)dataGridView_Computers.SelectedRows[0].Cells[0].Value;
                sn = (string)dataGridView_Computers.SelectedRows[0].Cells[1].Value;
            }
            catch { canCreate = false; }

            if (!canCreate)
                try
                {
                    id = (int)dataGridView_Computers.Rows[dataGridView_Computers.SelectedCells[0].RowIndex].Cells[0].Value;
                    sn = (string)dataGridView_Computers.Rows[dataGridView_Computers.SelectedCells[0].RowIndex].Cells[1].Value;
                }
                catch { return; }



            FormComponentsSE form = new FormComponentsSE(id, sn);
            form.ShowDialog();
            DGV_CatalogUpdate();
        }

        private void dataGridView_Catalog_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView_Catalog.CancelEdit();
            DGV_CatalogUpdate();
        }

        private void dataGridView_Catalog_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Не допустимый тип данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            dataGridView_Catalog.CancelEdit();
            DGV_CatalogUpdate();
        }

        private void dataGridView_Computers_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Не допустимый тип данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            dataGridView_Computers.CancelEdit();
            DGV_ComputersUpdate();
        }

        private void button_tableCatalog_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Name == "button_tableCatalog")
            {
                textBox_idCatalog.Clear();
                textBox_ipCatalog.Clear();
                textBox_maskCatalog.Clear();
                textBox_netCatalog.Clear();
                textBox_locationCatalog.Clear();
                textBox_majorCatalog.Clear();
                textBox_nameCatalog.Clear();
                textBox_typeCatalog.Clear();
            }
            else
            {
                textBox_ipComputers.Clear();
                textBox_maskComputers.Clear();
                textBox_netComputers.Clear();
                textBox_locationComputers.Clear();
                textBox_majorComputers.Clear();
            }
        }
    }
}
