﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Accounting
{
    public partial class FormComponentsSE : Form
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename = |DataDirectory|\Database_Equipment.mdf;Integrated Security=True";
        int computerId;
        string computerSn;
        bool canChange = false;

        public FormComponentsSE(int id, string sn)
        {
            computerId = id;
            computerSn = sn;
            InitializeComponent();
            Text = "Компоненты компьютера " + computerSn;
        }

        void ColorCells(DataGridView data)
        {
            for (int i = 0; i < data.RowCount; i++)
            {
                if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value < DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.Red;
                else if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value > DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkGreen;
                else data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkOrange;
            }
        }

        void DGV_ComponentsUpdate()
        {
            canChange = false;
            string request = String.Format(@"
            SELECT * FROM [Components] WHERE Computer_id = {0}",
            computerId);

            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(request, connection);

            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView_Components.DataSource = table;

            dataGridView_Components.Columns["Id"].Visible = false;

            dataGridView_Components.Columns["Computer_id"].Visible = false;

            dataGridView_Components.Columns["S_N"].HeaderText = "Серийный номер";
            dataGridView_Components.Columns["Name"].HeaderText = "Наименование";
            dataGridView_Components.Columns["Date_guaranty"].HeaderText = "Истечение гарантии";

            ColorCells(dataGridView_Components);

            dataGridView_Components.Columns["Seller"].Visible = false;
            dataGridView_Components.Columns["Type"].HeaderText = "Тип";
            dataGridView_Components.Columns["Date_entry"].Visible = false;
            dataGridView_Components.Columns["Price"].Visible = false;
            dataGridView_Components.Columns["Location"].Visible = false;
            dataGridView_Components.Columns["Major"].HeaderText = "Ответственный";

            //dataGridView_Components.ClearSelection();
            canChange = true;
        }

        private void dataGridView_Catalog_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!canChange) return;
            dataGridView_Components.CancelEdit();
            DGV_ComponentsUpdate();
        }

        private void FormComponentsSE_Load(object sender, EventArgs e)
        {
            DGV_ComponentsUpdate();
        }

        private void button_addComponents_Click(object sender, EventArgs e)
        {
            FormChangeComponentsSE form = new FormChangeComponentsSE(computerId, computerSn);

            if (form.ShowDialog() == DialogResult.OK)
                DGV_ComponentsUpdate();
        }

        private void dataGridView_Components_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (MessageBox.Show("Удалить комплектующую из компьютера?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
            {
                e.Cancel = true;
                return;
            }

            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = String.Format(@"
                UPDATE [Components] SET Computer_id = NULL, Location = N'Склад' WHERE Id = {0}", e.Row.Cells[0].Value);

                SqlCommand command = new SqlCommand(request, connect);
                command.ExecuteNonQuery();
            }
        }

        private void dataGridView_Components_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Не допустимый тип данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            dataGridView_Components.CancelEdit();
            DGV_ComponentsUpdate();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Удалить комплектующую из компьютера?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
                return;

            DataGridView dataGrid = dataGridView_Components;

            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = String.Format(@"
                UPDATE [Components] SET Computer_id = NULL, Location = N'Склад' WHERE Id = {0}", dataGrid.SelectedRows[0].Cells[0].Value);

                SqlCommand command = new SqlCommand(request, connect);
                command.ExecuteNonQuery();
            }

            DGV_ComponentsUpdate();
        }
    }
}
