﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Accounting
{
    public partial class FormMTO : Form
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename = |DataDirectory|\Database_Equipment.mdf;Integrated Security=True";
        bool canChange = true;

        public FormMTO()
        {
            InitializeComponent();
        }

        void ColorCells(DataGridView data)
        {
            for (int i = 0; i < data.RowCount; i++)
            {
                if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value < DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.Red;
                else if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value > DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkGreen;
                else data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkOrange;
            }
        }

        void DGV_ComputersUpdate()
        {
            canChange = false;
            string request = String.Format(@"SELECT * FROM [Computers] 
            WHERE Location LIKE N'%{0}%' AND Major LIKE N'%{1}%' AND Type LIKE N'%{2}%' AND Date_guaranty >= '{3}'",
            textBox_locationComputers.Text, textBox_majorComputers.Text,
            textBox_typeComputers.Text, dateTimePicker_computers.Value.ToString("yyyyMMdd"));

            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(request, connection);

            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView_Computers.DataSource = table;

            dataGridView_Computers.Columns["Id"].Visible = false;

            dataGridView_Computers.Columns["Net_name"].Visible = false;
            dataGridView_Computers.Columns["IP"].Visible = false;
            dataGridView_Computers.Columns["Mask_subnet"].Visible = false;

            dataGridView_Computers.Columns["S_N"].HeaderText = "Серийный номер";
            dataGridView_Computers.Columns["Name"].HeaderText = "Наименование";
            dataGridView_Computers.Columns["Date_guaranty"].HeaderText = "Истечение гарантии";

            ColorCells(dataGridView_Computers);

            dataGridView_Computers.Columns["Seller"].HeaderText = "Продавец";
            dataGridView_Computers.Columns["Type"].HeaderText = "Тип";
            dataGridView_Computers.Columns["Date_entry"].HeaderText = "Дата поступления";
            dataGridView_Computers.Columns["Price"].HeaderText = "Цена";
            dataGridView_Computers.Columns["Location"].HeaderText = "Размещение";
            dataGridView_Computers.Columns["Major"].HeaderText = "Ответственный";

            //dataGridView_Computers.ClearSelection();
            //dataGridView_Computers
            canChange = true;
        }
        void DGV_ComponentsUpdate()
        {
            canChange = false;
            string request = String.Format(@"SELECT * FROM [Components] 
            WHERE Location LIKE N'%{0}%' AND Major LIKE N'%{1}%' AND Type LIKE N'%{2}%' AND Date_guaranty >= '{3}'",
            textBox_locationComponents.Text, textBox_majorComponents.Text,
            textBox_typeComponents.Text, dateTimePicker_components.Value.ToString("yyyyMMdd"));

            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(request, connection);

            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView_Components.DataSource = table;

            dataGridView_Components.Columns["Id"].Visible = false;
            dataGridView_Components.Columns["Computer_id"].Visible = false;

            dataGridView_Components.Columns["S_N"].HeaderText = "Серийный номер";
            dataGridView_Components.Columns["Name"].HeaderText = "Наименование";
            dataGridView_Components.Columns["Date_guaranty"].HeaderText = "Истечение гарантии";

            ColorCells(dataGridView_Components);

            dataGridView_Components.Columns["Seller"].HeaderText = "Продавец";
            dataGridView_Components.Columns["Type"].HeaderText = "Тип";
            dataGridView_Components.Columns["Date_entry"].HeaderText = "Дата поступления";
            dataGridView_Components.Columns["Price"].HeaderText = "Цена";
            dataGridView_Components.Columns["Location"].HeaderText = "Размещение";
            dataGridView_Components.Columns["Major"].HeaderText = "Ответственный";

            //dataGridView_Components.ClearSelection();
            canChange = true;
        }
        void DGV_CatalogUpdate()
        {
            canChange = false;
            string request = String.Format(@"
         SELECT S_N, Name, Date_guaranty, Seller, Type, Date_entry, Price, Location, Major FROM [Components]
         WHERE Location LIKE N'%{0}%' AND Major LIKE N'%{1}%' AND Type LIKE N'%{2}%' AND Name LIKE N'%{3}%' UNION 
         SELECT S_N, Name, Date_guaranty, Seller, Type, Date_entry, Price, Location, Major FROM [Computers]
         WHERE Location LIKE N'%{0}%' AND Major LIKE N'%{1}%' AND Type LIKE N'%{2}%' AND Name LIKE N'%{3}%'",
         textBox_locationCatalog.Text, textBox_majorCatalog.Text, textBox_typeCatalog.Text, textBox_nameCatalog.Text);//нужны все поля? В методичке: ответственный, размещение, тип и наименование

            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(request, connection);

            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView_Catalog.DataSource = table;

            //dataGridView_Catalog.Columns["Id"].Visible = false;

            dataGridView_Catalog.Columns["S_N"].HeaderText = "Серийный номер";
            dataGridView_Catalog.Columns["Name"].HeaderText = "Наименование";
            dataGridView_Catalog.Columns["Date_guaranty"].HeaderText = "Истечение гарантии";

            ColorCells(dataGridView_Catalog);

            dataGridView_Catalog.Columns["Seller"].HeaderText = "Продавец";
            dataGridView_Catalog.Columns["Type"].HeaderText = "Тип";
            dataGridView_Catalog.Columns["Date_entry"].HeaderText = "Дата поступления";
            dataGridView_Catalog.Columns["Price"].HeaderText = "Цена";
            dataGridView_Catalog.Columns["Location"].HeaderText = "Размещение";
            dataGridView_Catalog.Columns["Major"].HeaderText = "Ответственный";

            //dataGridView_Catalog.ClearSelection();
            canChange = true;
        }

        private void FormMTO_Load(object sender, EventArgs e)
        {
            tabControl_MTO.SelectedIndex = 2;
            DGV_ComponentsUpdate();
            dataGridView_Components.Select();

            tabControl_MTO.SelectedIndex = 1;
            DGV_ComputersUpdate();
            dataGridView_Computers.Select();

            tabControl_MTO.SelectedIndex = 0;
            DGV_CatalogUpdate();
            dataGridView_Catalog.Select();
        }

        private void Catalog_TextChanged(object sender, EventArgs e)
        {
            DGV_CatalogUpdate();
        }

        private void Computers_TextChanged(object sender, EventArgs e)
        {
            DGV_ComputersUpdate();
        }

        private void Components_TextChanged(object sender, EventArgs e)
        {
            DGV_ComponentsUpdate();
        }

        //COMPUTERS:

        private void dataGridView_Computers_CellValueChanged(object sender, DataGridViewCellEventArgs e)//вызывается при заполнении
        {
            if (!canChange) return;

            if (e.ColumnIndex == 8)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Computers] SET Location = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else if (e.ColumnIndex == 9)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Computers] SET Major = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else
            {
                MessageBox.Show("Редактирование недоступно", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                DGV_ComputersUpdate();
            }

            DGV_CatalogUpdate();
        }

        private void dataGridView_Computers_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (MessageBox.Show("Удалить компьютер?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
            {
                e.Cancel = true;
                return;
            }


            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = String.Format(@"
                DELETE FROM [Computers] WHERE Id = {0}", e.Row.Cells[0].Value);

                SqlCommand command = new SqlCommand(request, connect);
                command.ExecuteNonQuery();
            }
            DGV_CatalogUpdate();
        }

        private void dataGridView_Computers_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Не допустимый тип данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            dataGridView_Computers.CancelEdit();
            DGV_ComputersUpdate();
        }

        // COMPONENTS:

        private void dataGridView_Components_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (MessageBox.Show("Удалить комплектующую?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
            {
                e.Cancel = true;
                return;
            }

            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = String.Format(@"
                DELETE FROM [Components] WHERE Id = {0}", e.Row.Cells[0].Value);

                SqlCommand command = new SqlCommand(request, connect);
                command.ExecuteNonQuery();
            }
            DGV_CatalogUpdate();
        }

        private void dataGridView_Components_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!canChange) return;

            if (e.ColumnIndex == 8)
            {
                DataGridView table = (DataGridView)sender;

                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    string request = String.Format(@"
                        SELECT Id FROM [Computers] WHERE S_N = N'{0}'",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();

                        request = String.Format(@"
                        UPDATE [Components] SET Location = N'{0}', Computer_id = {1} WHERE Id = {2}",
                        table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, reader.GetInt32(0), table.Rows[e.RowIndex].Cells[0].Value);

                        command = new SqlCommand(request, connect);
                        reader.Close();
                        command.ExecuteNonQuery();

                    }
                    else
                        try
                        {
                            if ((string)table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != "Склад")
                            {
                                MessageBox.Show("Не существующее размещение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                dataGridView_Components.CancelEdit();
                                DGV_ComponentsUpdate();
                                return;
                            }
                            else
                            {
                                using (SqlConnection connectStorage = new SqlConnection(connectionString))
                                {
                                    connect.Open();
                                    string requestStorage = String.Format(@"
                                    UPDATE [Components] SET Location = N'Склад', Computer_id = NULL WHERE Id = {0}",
                                    table.Rows[e.RowIndex].Cells[0].Value);

                                    SqlCommand commandStorage = new SqlCommand(request, connect);
                                    commandStorage.ExecuteNonQuery();
                                }
                                DGV_ComponentsUpdate();
                                DGV_CatalogUpdate();
                                return;
                            }
                        }
                        catch
                        {
                            MessageBox.Show("Не существующее размещение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            dataGridView_Components.CancelEdit();
                            DGV_ComponentsUpdate();
                            return;
                        }
                }
            }
            else if (e.ColumnIndex == 9)
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();

                    DataGridView table = (DataGridView)sender;

                    string request = String.Format(@"
                    UPDATE [Components] SET Major = N'{0}' WHERE Id = {1}",
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value, table.Rows[e.RowIndex].Cells[0].Value);

                    SqlCommand command = new SqlCommand(request, connect);
                    command.ExecuteNonQuery();
                }
            else
            {
                MessageBox.Show("Редактирование недоступно", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                DGV_ComponentsUpdate();
            }

            DGV_CatalogUpdate();
        }

        private void dataGridView_Components_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Не допустимый тип данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            dataGridView_Components.CancelEdit();
            DGV_ComponentsUpdate();
        }

        //СОЗДАНИЕ КОМПЬЮТЕРОВ И КОМПОНЕНТОВ:

        private void button_createComputers_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            List<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>()
            {
                new KeyValuePair<int, string>(-1, "Склад")
            };

            //List<string> idComputers = new List<string>();
            //List<string> idComponents = new List<string>();
            List<string> idCom = new List<string>();

            //список серийных номеров компьютеров и комплектующик + список размещения для комплектующих
            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = @"SELECT S_N, Id FROM [Computers]";
                SqlCommand command = new SqlCommand(request, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                    while (reader.Read())
                    {
                        string sn = reader.GetString(0);
                        int id = reader.GetInt32(1);

                        //idComputers.Add(sn);
                        idCom.Add(sn);
                        list.Add(new KeyValuePair<int, string>(id, "Компьютер " + sn));
                    }
                reader.Close();

                //=====================

                request = @"SELECT S_N FROM [Components]";
                command = new SqlCommand(request, connect);
                reader = command.ExecuteReader();

                if (reader.HasRows)
                    while (reader.Read())
                    {
                        string sn = reader.GetString(0);
                        //int id = reader.GetInt32(1);

                        //idComponents.Add(sn);
                        idCom.Add(sn);
                    }
                reader.Close();
            }

            FormCreateMTO form = new FormCreateMTO();

            form.idComputers = idCom;//idComputers;
            form.idComponents = idCom;//idComponents;

            //список продавцов
            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = @"SELECT Seller FROM [Computers] UNION SELECT Seller FROM [Components]";
                SqlCommand command = new SqlCommand(request, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                    while (reader.Read())
                    {
                        string seller = reader.GetString(0);

                        if (!form.textBox_seller.Items.Contains(seller)) form.textBox_seller.Items.Add(seller);
                    }
                reader.Close();
            }
            //список ответственных
            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = @"SELECT Major FROM [Computers] UNION SELECT Major FROM [Components]";
                SqlCommand command = new SqlCommand(request, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                    while (reader.Read())
                    {
                        string major = reader.GetString(0);

                        if (!form.textBox_major.Items.Contains(major)) form.textBox_major.Items.Add(major);
                    }
                reader.Close();
            }
            //список размещения для компьютеров
            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = @"SELECT Location FROM [Computers]";
                SqlCommand command = new SqlCommand(request, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                    while (reader.Read())
                    {
                        string location = reader.GetString(0);

                        if (!form.textBox_location.Items.Contains(location)) form.textBox_location.Items.Add(location);
                    }
                reader.Close();
            }
            //======================
            if (button.Name == "button_createComputers")
            {
                form.Text = "Регистрация компьютера";
                if (form.ShowDialog() == DialogResult.OK)
                    using (SqlConnection connect = new SqlConnection(connectionString))
                    {
                        connect.Open();
                        /*
                        //проверка на существующий серийный номер
                        string request = String.Format(@"
                            SELECT S_N FROM [Components] WHERE S_N = N'{0}'",
                            form.textBox_id.Text);
                        SqlCommand command = new SqlCommand(request, connect);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows) return;*/
                        string request;

                        request = String.Format(@"
                        INSERT INTO [Computers] (S_N, Name, Date_guaranty, Seller, Type, Date_entry, Price, Location, Major)
                        VALUES(N'{0}', N'{1}', '{2}', N'{3}', N'{4}', '{5}', {6}, N'{7}', N'{8}')",
                        form.textBox_id.Text, form.textBox_name.Text, form.dateTimePicker_guaranty.Value.ToString("yyyyMMdd"),
                        form.textBox_seller.Text, form.textBox_type.Text, form.dateTimePicker_entry.Value.ToString("yyyyMMdd"),
                        Convert.ToInt32(form.textBox_price.Text), form.textBox_location.Text, form.textBox_major.Text);

                        SqlCommand command = new SqlCommand(request, connect);
                        //reader.Close();
                        command.ExecuteNonQuery();

                        DGV_ComputersUpdate();
                        DGV_CatalogUpdate();
                    }
            }
            else
            {
                form.LocationComponents(list);
                form.Text = "Регистрация комплектующей";
                if (form.ShowDialog() == DialogResult.OK)
                    using (SqlConnection connect = new SqlConnection(connectionString))
                    {
                        connect.Open();
                        /*
                        //проверка на существующий серийный номер
                        string request = String.Format(@"
                            SELECT S_N FROM [Components] WHERE S_N = N'{0}'",
                            form.textBox_id.Text);
                        SqlCommand command = new SqlCommand(request, connect);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows) return;*/
                        string request;

                        int numberComputer = (int)form.textBox_location.SelectedValue;
                        //string snComputer = ((KeyValuePair<int, string>)form.textBox_location.SelectedItem).Value;
                        string snComputer = ((KeyValuePair<int, string>)form.textBox_location.SelectedItem).Value.Remove(0, 10);

                        if (numberComputer == -1)
                            request = String.Format(@"
                            INSERT INTO [Components] (S_N, Name, Date_guaranty, Seller, Type, Date_entry, Price, Location, Major)
                            VALUES(N'{0}', N'{1}', '{2}', N'{3}', N'{4}', '{5}', {6}, N'Склад', N'{7}')",
                            form.textBox_id.Text, form.textBox_name.Text, form.dateTimePicker_guaranty.Value.ToString("yyyyMMdd"),
                            form.textBox_seller.Text, form.textBox_type.Text, form.dateTimePicker_entry.Value.ToString("yyyyMMdd"),
                            Convert.ToInt32(form.textBox_price.Text), form.textBox_major.Text);
                        else
                            request = String.Format(@"
                            INSERT INTO [Components] (S_N, Name, Date_guaranty, Seller, Type, Date_entry, Price, Location, Major, Computer_id)
                            VALUES(N'{0}', N'{1}', '{2}', N'{3}', N'{4}', '{5}', {6}, N'{7}', N'{8}', {9})",
                            form.textBox_id.Text, form.textBox_name.Text, form.dateTimePicker_guaranty.Value.ToString("yyyyMMdd"),
                            form.textBox_seller.Text, form.textBox_type.Text, form.dateTimePicker_entry.Value.ToString("yyyyMMdd"),
                            Convert.ToInt32(form.textBox_price.Text), snComputer, form.textBox_major.Text, numberComputer);

                        SqlCommand command = new SqlCommand(request, connect);
                        //reader.Close();
                        command.ExecuteNonQuery();
                        //==========================
                        DGV_ComponentsUpdate();
                        DGV_CatalogUpdate();

                    }
            }
        }

        private void dataGridView_Catalog_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView_Catalog.CancelEdit();
            DGV_CatalogUpdate();
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            string nameTable, nameParam;
            if (tabControl_MTO.SelectedIndex == 1)
            {
                nameTable = "Computers";
                nameParam = "компьютер?";
            }
            else
            {
                nameTable = "Components";
                nameParam = "комплектующую?";
            }

            DataGridView dataGrid;
            if (tabControl_MTO.SelectedIndex == 1)
                dataGrid = dataGridView_Computers;
            else dataGrid = dataGridView_Components;

            try { int id = (int)dataGrid.SelectedRows[0].Cells[0].Value; }
            catch { return; }

            if (MessageBox.Show("Удалить " + nameParam, "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
                return;

            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = String.Format(@"
                DELETE FROM [{1}] WHERE Id = {0}", dataGrid.SelectedRows[0].Cells[0].Value, nameTable);

                SqlCommand command = new SqlCommand(request, connect);
                command.ExecuteNonQuery();
            }

            if (tabControl_MTO.SelectedIndex == 1) DGV_ComputersUpdate();
            else DGV_ComponentsUpdate();

            DGV_CatalogUpdate();
        }

        private void button_tableCatalog_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Name == "button_tableCatalog")
            {
                textBox_locationCatalog.Clear();
                textBox_majorCatalog.Clear();
                textBox_nameCatalog.Clear();
                textBox_typeCatalog.Clear();
            }
            else if (button.Name == "button_tableComputers")
            {
                textBox_locationComputers.Clear();
                textBox_majorComputers.Clear();
                textBox_typeComputers.Clear();
            }
            else
            {
                textBox_locationComponents.Clear();
                textBox_majorComponents.Clear();
                textBox_typeComponents.Clear();
            }
        }
    }
}
