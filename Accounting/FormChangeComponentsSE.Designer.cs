﻿namespace Accounting
{
    partial class FormChangeComponentsSE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChangeComponentsSE));
            this.dataGridView_Components = new System.Windows.Forms.DataGridView();
            this.button_addComponents = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_tableComponents = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_locationComponents = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_majorComponents = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_typeComponents = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Components)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView_Components
            // 
            this.dataGridView_Components.AllowUserToAddRows = false;
            this.dataGridView_Components.AllowUserToDeleteRows = false;
            this.dataGridView_Components.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Components.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Components.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Components.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Components.Location = new System.Drawing.Point(12, 12);
            this.dataGridView_Components.MultiSelect = false;
            this.dataGridView_Components.Name = "dataGridView_Components";
            this.dataGridView_Components.ReadOnly = true;
            this.dataGridView_Components.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_Components.Size = new System.Drawing.Size(790, 350);
            this.dataGridView_Components.TabIndex = 2;
            this.dataGridView_Components.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Components_CellValueChanged);
            this.dataGridView_Components.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_Components_DataError);
            // 
            // button_addComponents
            // 
            this.button_addComponents.AutoSize = true;
            this.button_addComponents.Location = new System.Drawing.Point(554, 368);
            this.button_addComponents.Name = "button_addComponents";
            this.button_addComponents.Size = new System.Drawing.Size(141, 23);
            this.button_addComponents.TabIndex = 0;
            this.button_addComponents.Text = "Добавить к компьютеру";
            this.button_addComponents.UseVisualStyleBackColor = true;
            this.button_addComponents.Click += new System.EventHandler(this.button_addComponents_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(714, 368);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_cancel.TabIndex = 1;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_tableComponents
            // 
            this.button_tableComponents.Location = new System.Drawing.Point(447, 379);
            this.button_tableComponents.Name = "button_tableComponents";
            this.button_tableComponents.Size = new System.Drawing.Size(59, 48);
            this.button_tableComponents.TabIndex = 30;
            this.button_tableComponents.Text = "Сброс";
            this.button_tableComponents.UseVisualStyleBackColor = true;
            this.button_tableComponents.Click += new System.EventHandler(this.button_tableComponents_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_locationComponents);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox_majorComponents);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox_typeComponents);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Location = new System.Drawing.Point(12, 368);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(429, 65);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск";
            // 
            // textBox_locationComponents
            // 
            this.textBox_locationComponents.Location = new System.Drawing.Point(6, 39);
            this.textBox_locationComponents.Name = "textBox_locationComponents";
            this.textBox_locationComponents.Size = new System.Drawing.Size(100, 20);
            this.textBox_locationComponents.TabIndex = 0;
            this.textBox_locationComponents.TextChanged += new System.EventHandler(this.textBox_locationComponents_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Размещение";
            // 
            // textBox_majorComponents
            // 
            this.textBox_majorComponents.Location = new System.Drawing.Point(161, 39);
            this.textBox_majorComponents.Name = "textBox_majorComponents";
            this.textBox_majorComponents.Size = new System.Drawing.Size(100, 20);
            this.textBox_majorComponents.TabIndex = 1;
            this.textBox_majorComponents.TextChanged += new System.EventHandler(this.textBox_locationComponents_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(162, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Ответственный";
            // 
            // textBox_typeComponents
            // 
            this.textBox_typeComponents.Location = new System.Drawing.Point(317, 39);
            this.textBox_typeComponents.Name = "textBox_typeComponents";
            this.textBox_typeComponents.Size = new System.Drawing.Size(100, 20);
            this.textBox_typeComponents.TabIndex = 2;
            this.textBox_typeComponents.TextChanged += new System.EventHandler(this.textBox_locationComponents_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(318, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Тип";
            // 
            // FormChangeComponentsSE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(814, 442);
            this.Controls.Add(this.button_tableComponents);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_addComponents);
            this.Controls.Add(this.dataGridView_Components);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormChangeComponentsSE";
            this.Text = "Имеющиеся компонеты";
            this.Load += new System.EventHandler(this.FormChangeComponentsSE_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Components)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Components;
        private System.Windows.Forms.Button button_addComponents;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_tableComponents;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_locationComponents;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_majorComponents;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_typeComponents;
        private System.Windows.Forms.Label label12;
    }
}