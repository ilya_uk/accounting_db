﻿namespace Accounting
{
    partial class FormCreateMTO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCreateMTO));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_cancel = new System.Windows.Forms.Button();
            this.textBox_location = new System.Windows.Forms.ComboBox();
            this.textBox_major = new System.Windows.Forms.ComboBox();
            this.textBox_seller = new System.Windows.Forms.ComboBox();
            this.button_add = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker_entry = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_guaranty = new System.Windows.Forms.DateTimePicker();
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.textBox_type = new System.Windows.Forms.TextBox();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.textBox_id = new System.Windows.Forms.TextBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_cancel);
            this.groupBox1.Controls.Add(this.textBox_location);
            this.groupBox1.Controls.Add(this.textBox_major);
            this.groupBox1.Controls.Add(this.textBox_seller);
            this.groupBox1.Controls.Add(this.button_add);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dateTimePicker_entry);
            this.groupBox1.Controls.Add(this.dateTimePicker_guaranty);
            this.groupBox1.Controls.Add(this.textBox_price);
            this.groupBox1.Controls.Add(this.textBox_type);
            this.groupBox1.Controls.Add(this.textBox_name);
            this.groupBox1.Controls.Add(this.textBox_id);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(649, 239);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Добавление";
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(568, 210);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_cancel.TabIndex = 10;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // textBox_location
            // 
            this.textBox_location.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBox_location.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.textBox_location.DropDownWidth = 100;
            this.textBox_location.FormattingEnabled = true;
            this.textBox_location.Location = new System.Drawing.Point(206, 154);
            this.textBox_location.Name = "textBox_location";
            this.textBox_location.Size = new System.Drawing.Size(147, 21);
            this.textBox_location.TabIndex = 7;
            // 
            // textBox_major
            // 
            this.textBox_major.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBox_major.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.textBox_major.DropDownWidth = 100;
            this.textBox_major.FormattingEnabled = true;
            this.textBox_major.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBox_major.Location = new System.Drawing.Point(383, 154);
            this.textBox_major.Name = "textBox_major";
            this.textBox_major.Size = new System.Drawing.Size(136, 21);
            this.textBox_major.TabIndex = 8;
            // 
            // textBox_seller
            // 
            this.textBox_seller.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBox_seller.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.textBox_seller.FormattingEnabled = true;
            this.textBox_seller.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBox_seller.Location = new System.Drawing.Point(321, 59);
            this.textBox_seller.Name = "textBox_seller";
            this.textBox_seller.Size = new System.Drawing.Size(100, 21);
            this.textBox_seller.TabIndex = 3;
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(478, 210);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(75, 23);
            this.button_add.TabIndex = 9;
            this.button_add.Text = "Добавить";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 26);
            this.label9.TabIndex = 17;
            this.label9.Text = "Дата\r\nпоступления\r\n";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(226, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 26);
            this.label8.TabIndex = 16;
            this.label8.Text = "Истечение\r\nгарантии\r\n";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(380, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Ответственный";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(203, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Размещение";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(94, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Цена";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(437, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Тип";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(318, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Продавец";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Наименование";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 26);
            this.label1.TabIndex = 9;
            this.label1.Text = "Серийный\r\nномер";
            // 
            // dateTimePicker_entry
            // 
            this.dateTimePicker_entry.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_entry.Location = new System.Drawing.Point(6, 155);
            this.dateTimePicker_entry.Name = "dateTimePicker_entry";
            this.dateTimePicker_entry.Size = new System.Drawing.Size(75, 20);
            this.dateTimePicker_entry.TabIndex = 5;
            // 
            // dateTimePicker_guaranty
            // 
            this.dateTimePicker_guaranty.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_guaranty.Location = new System.Drawing.Point(226, 60);
            this.dateTimePicker_guaranty.Name = "dateTimePicker_guaranty";
            this.dateTimePicker_guaranty.Size = new System.Drawing.Size(75, 20);
            this.dateTimePicker_guaranty.TabIndex = 2;
            // 
            // textBox_price
            // 
            this.textBox_price.Location = new System.Drawing.Point(97, 155);
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.Size = new System.Drawing.Size(81, 20);
            this.textBox_price.TabIndex = 6;
            this.textBox_price.TextChanged += new System.EventHandler(this.textBox_id_TextChanged);
            // 
            // textBox_type
            // 
            this.textBox_type.Location = new System.Drawing.Point(440, 59);
            this.textBox_type.Name = "textBox_type";
            this.textBox_type.Size = new System.Drawing.Size(100, 20);
            this.textBox_type.TabIndex = 4;
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(106, 60);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(100, 20);
            this.textBox_name.TabIndex = 1;
            // 
            // textBox_id
            // 
            this.textBox_id.Location = new System.Drawing.Point(6, 60);
            this.textBox_id.Name = "textBox_id";
            this.textBox_id.Size = new System.Drawing.Size(75, 20);
            this.textBox_id.TabIndex = 0;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // FormCreateMTO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(674, 272);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormCreateMTO";
            this.Text = "Регистрация оборудования";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_add;
        internal System.Windows.Forms.DateTimePicker dateTimePicker_entry;
        internal System.Windows.Forms.DateTimePicker dateTimePicker_guaranty;
        internal System.Windows.Forms.TextBox textBox_price;
        internal System.Windows.Forms.TextBox textBox_type;
        internal System.Windows.Forms.TextBox textBox_name;
        internal System.Windows.Forms.TextBox textBox_id;
        internal System.Windows.Forms.ComboBox textBox_seller;
        internal System.Windows.Forms.ComboBox textBox_major;
        internal System.Windows.Forms.ComboBox textBox_location;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}