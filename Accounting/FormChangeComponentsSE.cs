﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Accounting
{
    public partial class FormChangeComponentsSE : Form
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename = |DataDirectory|\Database_Equipment.mdf;Integrated Security=True";
        int computerId;
        string computerSn;
        bool canChange = false;

        public FormChangeComponentsSE(int id, string sn)
        {
            computerId = id;
            computerSn = sn;
            InitializeComponent();
        }

        void ColorCells(DataGridView data)
        {
            for (int i = 0; i < data.RowCount; i++)
            {
                if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value < DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.Red;
                else if ((DateTime)data.Rows[i].Cells["Date_guaranty"].Value > DateTime.Now.Date)
                    data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkGreen;
                else data.Rows[i].Cells["Date_guaranty"].Style.ForeColor = Color.DarkOrange;
            }
        }

        void DGV_ComponentsUpdate()
        {
            canChange = false;

            string request = String.Format(@"
            SELECT * FROM [Components] WHERE Location != N'{0}' AND Location LIKE N'%{1}%'
            AND Major LIKE N'%{2}%' AND Type LIKE N'%{3}%'",
            computerSn, textBox_locationComponents.Text, textBox_majorComponents.Text, textBox_typeComponents.Text);

            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(request, connection);

            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridView_Components.DataSource = table;

            dataGridView_Components.Columns["Id"].Visible = false;

            dataGridView_Components.Columns["Computer_id"].Visible = false;

            dataGridView_Components.Columns["S_N"].HeaderText = "Серийный номер";
            dataGridView_Components.Columns["Name"].HeaderText = "Наименование";
            dataGridView_Components.Columns["Date_guaranty"].HeaderText = "Истечение гарантии";

            ColorCells(dataGridView_Components);

            dataGridView_Components.Columns["Seller"].Visible = false;
            dataGridView_Components.Columns["Type"].HeaderText = "Тип";
            dataGridView_Components.Columns["Date_entry"].HeaderText = "Дата поступления";
            dataGridView_Components.Columns["Price"].Visible = false;
            dataGridView_Components.Columns["Location"].HeaderText = "Размещение";
            dataGridView_Components.Columns["Major"].HeaderText = "Ответственный";

            //dataGridView_Components.ClearSelection();
            canChange = true;
        }

        private void FormChangeComponentsSE_Load(object sender, EventArgs e)
        {
            DGV_ComponentsUpdate();
        }

        private void button_addComponents_Click(object sender, EventArgs e)
        {
            int id = 0;

            try { id = (int)dataGridView_Components.SelectedRows[0].Cells[0].Value; }
            catch { return; }

            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                string request = String.Format(@"
                UPDATE [Components] SET Computer_id = {0}, Location = N'{1}' WHERE Id = {2}", computerId, computerSn, id);

                SqlCommand command = new SqlCommand(request, connect);
                command.ExecuteNonQuery();
            }

            DialogResult = DialogResult.OK;
        }

        private void dataGridView_Components_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!canChange) return;
            dataGridView_Components.CancelEdit();
            DGV_ComponentsUpdate();
        }

        private void dataGridView_Components_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Не допустимый тип данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            dataGridView_Components.CancelEdit();
            DGV_ComponentsUpdate();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void textBox_locationComponents_TextChanged(object sender, EventArgs e)
        {
            DGV_ComponentsUpdate();
        }

        private void button_tableComponents_Click(object sender, EventArgs e)
        {
            textBox_locationComponents.Clear();
            textBox_majorComponents.Clear();
            textBox_typeComponents.Clear();
        }
    }
}
