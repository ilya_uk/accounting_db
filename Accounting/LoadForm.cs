﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Accounting
{
    public partial class LoadForm : Form
    {
        const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename = |DataDirectory|\Database_Equipment.mdf;Integrated Security=True";

        public LoadForm()
        {
            InitializeComponent();
        }

        void ShowText()
        {
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                connection.Close();

                DialogResult = DialogResult.OK;
            }
            catch
            {
                DialogResult = DialogResult.Cancel;
            }   
        }

        private void LoadFormcs_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ShowText();
            timer1.Stop();
        }
    }
}
