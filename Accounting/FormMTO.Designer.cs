﻿namespace Accounting
{
    partial class FormMTO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMTO));
            this.tabControl_MTO = new System.Windows.Forms.TabControl();
            this.tabPage_Catalog = new System.Windows.Forms.TabPage();
            this.button_tableCatalog = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox_locationCatalog = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_nameCatalog = new System.Windows.Forms.TextBox();
            this.textBox_majorCatalog = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_typeCatalog = new System.Windows.Forms.TextBox();
            this.dataGridView_Catalog = new System.Windows.Forms.DataGridView();
            this.tabPage_Computers = new System.Windows.Forms.TabPage();
            this.button_tableComputers = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox_locationComputers = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_majorComputers = new System.Windows.Forms.TextBox();
            this.dateTimePicker_computers = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_typeComputers = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button_delete = new System.Windows.Forms.Button();
            this.button_createComputers = new System.Windows.Forms.Button();
            this.dataGridView_Computers = new System.Windows.Forms.DataGridView();
            this.tabPage_Components = new System.Windows.Forms.TabPage();
            this.button_tableComponents = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_locationComponents = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_majorComponents = new System.Windows.Forms.TextBox();
            this.dateTimePicker_components = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_typeComponents = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button_deleteComponents = new System.Windows.Forms.Button();
            this.button_createComponents = new System.Windows.Forms.Button();
            this.dataGridView_Components = new System.Windows.Forms.DataGridView();
            this.tabControl_MTO.SuspendLayout();
            this.tabPage_Catalog.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Catalog)).BeginInit();
            this.tabPage_Computers.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Computers)).BeginInit();
            this.tabPage_Components.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Components)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl_MTO
            // 
            this.tabControl_MTO.Controls.Add(this.tabPage_Catalog);
            this.tabControl_MTO.Controls.Add(this.tabPage_Computers);
            this.tabControl_MTO.Controls.Add(this.tabPage_Components);
            this.tabControl_MTO.Location = new System.Drawing.Point(16, 15);
            this.tabControl_MTO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl_MTO.Name = "tabControl_MTO";
            this.tabControl_MTO.SelectedIndex = 0;
            this.tabControl_MTO.Size = new System.Drawing.Size(1200, 524);
            this.tabControl_MTO.TabIndex = 0;
            // 
            // tabPage_Catalog
            // 
            this.tabPage_Catalog.Controls.Add(this.button_tableCatalog);
            this.tabPage_Catalog.Controls.Add(this.groupBox3);
            this.tabPage_Catalog.Controls.Add(this.dataGridView_Catalog);
            this.tabPage_Catalog.Location = new System.Drawing.Point(4, 25);
            this.tabPage_Catalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Catalog.Name = "tabPage_Catalog";
            this.tabPage_Catalog.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Catalog.Size = new System.Drawing.Size(1192, 495);
            this.tabPage_Catalog.TabIndex = 0;
            this.tabPage_Catalog.Text = "Оборудование";
            this.tabPage_Catalog.UseVisualStyleBackColor = true;
            // 
            // button_tableCatalog
            // 
            this.button_tableCatalog.Location = new System.Drawing.Point(799, 422);
            this.button_tableCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_tableCatalog.Name = "button_tableCatalog";
            this.button_tableCatalog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button_tableCatalog.Size = new System.Drawing.Size(79, 59);
            this.button_tableCatalog.TabIndex = 5;
            this.button_tableCatalog.Text = "Сброс";
            this.button_tableCatalog.UseVisualStyleBackColor = true;
            this.button_tableCatalog.Click += new System.EventHandler(this.button_tableCatalog_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox_locationCatalog);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.textBox_nameCatalog);
            this.groupBox3.Controls.Add(this.textBox_majorCatalog);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textBox_typeCatalog);
            this.groupBox3.Location = new System.Drawing.Point(4, 409);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(787, 80);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Поиск";
            // 
            // textBox_locationCatalog
            // 
            this.textBox_locationCatalog.Location = new System.Drawing.Point(8, 48);
            this.textBox_locationCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_locationCatalog.Name = "textBox_locationCatalog";
            this.textBox_locationCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_locationCatalog.TabIndex = 0;
            this.textBox_locationCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(632, 25);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Наименование";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Размещение";
            // 
            // textBox_nameCatalog
            // 
            this.textBox_nameCatalog.Location = new System.Drawing.Point(631, 48);
            this.textBox_nameCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_nameCatalog.Name = "textBox_nameCatalog";
            this.textBox_nameCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_nameCatalog.TabIndex = 3;
            this.textBox_nameCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChanged);
            // 
            // textBox_majorCatalog
            // 
            this.textBox_majorCatalog.Location = new System.Drawing.Point(215, 48);
            this.textBox_majorCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_majorCatalog.Name = "textBox_majorCatalog";
            this.textBox_majorCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_majorCatalog.TabIndex = 1;
            this.textBox_majorCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(425, 25);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Тип";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(216, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ответственный";
            // 
            // textBox_typeCatalog
            // 
            this.textBox_typeCatalog.Location = new System.Drawing.Point(424, 48);
            this.textBox_typeCatalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_typeCatalog.Name = "textBox_typeCatalog";
            this.textBox_typeCatalog.Size = new System.Drawing.Size(132, 22);
            this.textBox_typeCatalog.TabIndex = 2;
            this.textBox_typeCatalog.TextChanged += new System.EventHandler(this.Catalog_TextChanged);
            // 
            // dataGridView_Catalog
            // 
            this.dataGridView_Catalog.AllowUserToAddRows = false;
            this.dataGridView_Catalog.AllowUserToDeleteRows = false;
            this.dataGridView_Catalog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Catalog.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Catalog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Catalog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Catalog.Location = new System.Drawing.Point(8, 7);
            this.dataGridView_Catalog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView_Catalog.MultiSelect = false;
            this.dataGridView_Catalog.Name = "dataGridView_Catalog";
            this.dataGridView_Catalog.ReadOnly = true;
            this.dataGridView_Catalog.RowHeadersWidth = 51;
            this.dataGridView_Catalog.Size = new System.Drawing.Size(1173, 394);
            this.dataGridView_Catalog.TabIndex = 4;
            this.dataGridView_Catalog.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Catalog_CellValueChanged);
            // 
            // tabPage_Computers
            // 
            this.tabPage_Computers.Controls.Add(this.button_tableComputers);
            this.tabPage_Computers.Controls.Add(this.groupBox2);
            this.tabPage_Computers.Controls.Add(this.button_delete);
            this.tabPage_Computers.Controls.Add(this.button_createComputers);
            this.tabPage_Computers.Controls.Add(this.dataGridView_Computers);
            this.tabPage_Computers.Location = new System.Drawing.Point(4, 25);
            this.tabPage_Computers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Computers.Name = "tabPage_Computers";
            this.tabPage_Computers.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Computers.Size = new System.Drawing.Size(1192, 495);
            this.tabPage_Computers.TabIndex = 1;
            this.tabPage_Computers.Text = "Компьютеры";
            this.tabPage_Computers.UseVisualStyleBackColor = true;
            // 
            // button_tableComputers
            // 
            this.button_tableComputers.Location = new System.Drawing.Point(799, 422);
            this.button_tableComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_tableComputers.Name = "button_tableComputers";
            this.button_tableComputers.Size = new System.Drawing.Size(79, 59);
            this.button_tableComputers.TabIndex = 7;
            this.button_tableComputers.Text = "Сброс";
            this.button_tableComputers.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_locationComputers);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox_majorComputers);
            this.groupBox2.Controls.Add(this.dateTimePicker_computers);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBox_typeComputers);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(4, 409);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(787, 80);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск";
            // 
            // textBox_locationComputers
            // 
            this.textBox_locationComputers.Location = new System.Drawing.Point(8, 48);
            this.textBox_locationComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_locationComputers.Name = "textBox_locationComputers";
            this.textBox_locationComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_locationComputers.TabIndex = 0;
            this.textBox_locationComputers.TextChanged += new System.EventHandler(this.Computers_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Размещение";
            // 
            // textBox_majorComputers
            // 
            this.textBox_majorComputers.Location = new System.Drawing.Point(215, 48);
            this.textBox_majorComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_majorComputers.Name = "textBox_majorComputers";
            this.textBox_majorComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_majorComputers.TabIndex = 1;
            this.textBox_majorComputers.TextChanged += new System.EventHandler(this.Computers_TextChanged);
            // 
            // dateTimePicker_computers
            // 
            this.dateTimePicker_computers.CustomFormat = "";
            this.dateTimePicker_computers.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_computers.Location = new System.Drawing.Point(635, 48);
            this.dateTimePicker_computers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePicker_computers.Name = "dateTimePicker_computers";
            this.dateTimePicker_computers.Size = new System.Drawing.Size(117, 22);
            this.dateTimePicker_computers.TabIndex = 3;
            this.dateTimePicker_computers.Value = new System.DateTime(2000, 4, 20, 0, 0, 0, 0);
            this.dateTimePicker_computers.ValueChanged += new System.EventHandler(this.Computers_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(216, 25);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ответственный";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(631, 25);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "Истечение гарантии";
            // 
            // textBox_typeComputers
            // 
            this.textBox_typeComputers.Location = new System.Drawing.Point(423, 48);
            this.textBox_typeComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_typeComputers.Name = "textBox_typeComputers";
            this.textBox_typeComputers.Size = new System.Drawing.Size(132, 22);
            this.textBox_typeComputers.TabIndex = 2;
            this.textBox_typeComputers.TextChanged += new System.EventHandler(this.Computers_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(424, 25);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Тип";
            // 
            // button_delete
            // 
            this.button_delete.Location = new System.Drawing.Point(1064, 423);
            this.button_delete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(117, 56);
            this.button_delete.TabIndex = 5;
            this.button_delete.Text = "Удалить";
            this.button_delete.UseVisualStyleBackColor = true;
            this.button_delete.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // button_createComputers
            // 
            this.button_createComputers.Location = new System.Drawing.Point(905, 422);
            this.button_createComputers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_createComputers.Name = "button_createComputers";
            this.button_createComputers.Size = new System.Drawing.Size(133, 57);
            this.button_createComputers.TabIndex = 4;
            this.button_createComputers.Text = "Добавить";
            this.button_createComputers.UseVisualStyleBackColor = true;
            this.button_createComputers.Click += new System.EventHandler(this.button_createComputers_Click);
            // 
            // dataGridView_Computers
            // 
            this.dataGridView_Computers.AllowUserToAddRows = false;
            this.dataGridView_Computers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Computers.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Computers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Computers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Computers.Location = new System.Drawing.Point(8, 7);
            this.dataGridView_Computers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView_Computers.MultiSelect = false;
            this.dataGridView_Computers.Name = "dataGridView_Computers";
            this.dataGridView_Computers.RowHeadersWidth = 51;
            this.dataGridView_Computers.Size = new System.Drawing.Size(1173, 394);
            this.dataGridView_Computers.TabIndex = 6;
            this.dataGridView_Computers.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Computers_CellValueChanged);
            this.dataGridView_Computers.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_Computers_DataError);
            this.dataGridView_Computers.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView_Computers_UserDeletingRow);
            // 
            // tabPage_Components
            // 
            this.tabPage_Components.Controls.Add(this.button_tableComponents);
            this.tabPage_Components.Controls.Add(this.groupBox1);
            this.tabPage_Components.Controls.Add(this.button_deleteComponents);
            this.tabPage_Components.Controls.Add(this.button_createComponents);
            this.tabPage_Components.Controls.Add(this.dataGridView_Components);
            this.tabPage_Components.Location = new System.Drawing.Point(4, 25);
            this.tabPage_Components.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage_Components.Name = "tabPage_Components";
            this.tabPage_Components.Size = new System.Drawing.Size(1192, 495);
            this.tabPage_Components.TabIndex = 2;
            this.tabPage_Components.Text = "Комплектующие";
            this.tabPage_Components.UseVisualStyleBackColor = true;
            // 
            // button_tableComponents
            // 
            this.button_tableComponents.Location = new System.Drawing.Point(799, 422);
            this.button_tableComponents.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_tableComponents.Name = "button_tableComponents";
            this.button_tableComponents.Size = new System.Drawing.Size(79, 59);
            this.button_tableComponents.TabIndex = 7;
            this.button_tableComponents.Text = "Сброс";
            this.button_tableComponents.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_locationComponents);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox_majorComponents);
            this.groupBox1.Controls.Add(this.dateTimePicker_components);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBox_typeComponents);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Location = new System.Drawing.Point(4, 409);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(787, 80);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск";
            // 
            // textBox_locationComponents
            // 
            this.textBox_locationComponents.Location = new System.Drawing.Point(8, 48);
            this.textBox_locationComponents.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_locationComponents.Name = "textBox_locationComponents";
            this.textBox_locationComponents.Size = new System.Drawing.Size(132, 22);
            this.textBox_locationComponents.TabIndex = 0;
            this.textBox_locationComponents.TextChanged += new System.EventHandler(this.Components_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 25);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Размещение";
            // 
            // textBox_majorComponents
            // 
            this.textBox_majorComponents.Location = new System.Drawing.Point(215, 48);
            this.textBox_majorComponents.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_majorComponents.Name = "textBox_majorComponents";
            this.textBox_majorComponents.Size = new System.Drawing.Size(132, 22);
            this.textBox_majorComponents.TabIndex = 1;
            this.textBox_majorComponents.TextChanged += new System.EventHandler(this.Components_TextChanged);
            // 
            // dateTimePicker_components
            // 
            this.dateTimePicker_components.CustomFormat = "";
            this.dateTimePicker_components.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_components.Location = new System.Drawing.Point(635, 48);
            this.dateTimePicker_components.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePicker_components.Name = "dateTimePicker_components";
            this.dateTimePicker_components.Size = new System.Drawing.Size(117, 22);
            this.dateTimePicker_components.TabIndex = 3;
            this.dateTimePicker_components.Value = new System.DateTime(2000, 4, 20, 0, 0, 0, 0);
            this.dateTimePicker_components.ValueChanged += new System.EventHandler(this.Components_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(216, 25);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Ответственный";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(631, 25);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(144, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "Истечение гарантии";
            // 
            // textBox_typeComponents
            // 
            this.textBox_typeComponents.Location = new System.Drawing.Point(423, 48);
            this.textBox_typeComponents.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_typeComponents.Name = "textBox_typeComponents";
            this.textBox_typeComponents.Size = new System.Drawing.Size(132, 22);
            this.textBox_typeComponents.TabIndex = 2;
            this.textBox_typeComponents.TextChanged += new System.EventHandler(this.Components_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(424, 25);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 17);
            this.label12.TabIndex = 27;
            this.label12.Text = "Тип";
            // 
            // button_deleteComponents
            // 
            this.button_deleteComponents.Location = new System.Drawing.Point(1049, 422);
            this.button_deleteComponents.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_deleteComponents.Name = "button_deleteComponents";
            this.button_deleteComponents.Size = new System.Drawing.Size(115, 57);
            this.button_deleteComponents.TabIndex = 5;
            this.button_deleteComponents.Text = "Удалить";
            this.button_deleteComponents.UseVisualStyleBackColor = true;
            this.button_deleteComponents.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // button_createComponents
            // 
            this.button_createComponents.Location = new System.Drawing.Point(904, 422);
            this.button_createComponents.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_createComponents.Name = "button_createComponents";
            this.button_createComponents.Size = new System.Drawing.Size(119, 59);
            this.button_createComponents.TabIndex = 4;
            this.button_createComponents.Text = "Добавить";
            this.button_createComponents.UseVisualStyleBackColor = true;
            this.button_createComponents.Click += new System.EventHandler(this.button_createComputers_Click);
            // 
            // dataGridView_Components
            // 
            this.dataGridView_Components.AllowUserToAddRows = false;
            this.dataGridView_Components.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Components.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Components.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Components.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Components.Location = new System.Drawing.Point(8, 7);
            this.dataGridView_Components.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView_Components.MultiSelect = false;
            this.dataGridView_Components.Name = "dataGridView_Components";
            this.dataGridView_Components.RowHeadersWidth = 51;
            this.dataGridView_Components.Size = new System.Drawing.Size(1173, 394);
            this.dataGridView_Components.TabIndex = 6;
            this.dataGridView_Components.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Components_CellValueChanged);
            this.dataGridView_Components.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_Components_DataError);
            this.dataGridView_Components.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView_Components_UserDeletingRow);
            // 
            // FormMTO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1232, 556);
            this.Controls.Add(this.tabControl_MTO);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "FormMTO";
            this.Text = "Материально-технический отдел";
            this.Load += new System.EventHandler(this.FormMTO_Load);
            this.tabControl_MTO.ResumeLayout(false);
            this.tabPage_Catalog.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Catalog)).EndInit();
            this.tabPage_Computers.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Computers)).EndInit();
            this.tabPage_Components.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Components)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl_MTO;
        private System.Windows.Forms.TabPage tabPage_Catalog;
        private System.Windows.Forms.TabPage tabPage_Computers;
        private System.Windows.Forms.TabPage tabPage_Components;
        private System.Windows.Forms.DataGridView dataGridView_Catalog;
        private System.Windows.Forms.DataGridView dataGridView_Computers;
        private System.Windows.Forms.DataGridView dataGridView_Components;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_majorCatalog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_locationCatalog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_majorComputers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_locationComputers;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_majorComponents;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_locationComponents;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_nameCatalog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_typeCatalog;
        private System.Windows.Forms.DateTimePicker dateTimePicker_computers;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_typeComputers;
        private System.Windows.Forms.DateTimePicker dateTimePicker_components;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_typeComponents;
        private System.Windows.Forms.Button button_createComputers;
        private System.Windows.Forms.Button button_createComponents;
        private System.Windows.Forms.Button button_delete;
        private System.Windows.Forms.Button button_deleteComponents;
        private System.Windows.Forms.Button button_tableCatalog;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_tableComputers;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_tableComponents;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}